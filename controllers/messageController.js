var MessageController = function(db)
{
	this.db = db;
}

MessageController.prototype.postTextMessage = function(username, password, locationId, textMessage, date)
{
	var db = this.db;
	return new Promise(function(resolve, reject)
	{
		db.generateMessageId().then(function(idMessage)
		{
			db.getUserId(username, password).then(function(userId)
			{
				if(userId != null)
				{
					db.addMessage(userId, locationId, idMessage, new Date(date)).then(function()
					{
						db.addTextMessage(idMessage, textMessage).then(function()
						{
							resolve(
							{
								status: 201, 
								body:
								{
									username: username, 
									idMessage: idMessage,
									locationId: locationId, 
									textMessage: textMessage, 
									date: date
								}
							});
						});
					});
				}
				else
				{
					resolve(
					{
						status: '401',
						body: {error: 'invalid username or password'}
					});
				}
			});
		});
	});
}

MessageController.prototype.modifyTextMessage = function(username, password, messageId, textMessage)
{
	var db = this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			db.getMessage(messageId).then(function(message)
			{
				if(userId != null && message != undefined && userId == message.idUser)
				{
					db.modifyTextMessage(messageId, textMessage).then(function()
					{
						resolve(
						{
							status: 200,
							body: 
							{
								idMessage: messageId,
								textMessage: textMessage
							}
						});
					});
				}
				else
				{
					resolve(
					{
						status: 400,
						body: {error: 'Invalid User or message'}
					});
				}
			});
			
		});
	});
}

MessageController.prototype.deleteMessage = function(username, password, messageId, type)
{
	var db = this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			db.getMessage(messageId).then(function(message)
			{
				if(userId != null && message != undefined && userId == message.idUser)
				{
					db.deleteMessage(messageId, type).then(function()
					{
						resolve(
						{
							status: 200,
							body: 
							{
								idMessage: messageId
							}
						});
					});
				}
				else
				{
					resolve(
					{
						status: 400,
						body: {error: 'Invalid User or message'}
					});
				}
			});
			
		});
	});
}


MessageController.prototype.getMessagesOrderedByDate = function(username, password, type, targetUsername)
{
	var db =this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			if(userId != null)
			{
				db.getUserId(targetUsername).then(function(targetUserId)
				{
					db.getMessagesOrderedByDate(targetUserId, type).then(function(textMessages)
					{
						resolve(
						{
							status: 200,
							body: 
							{
								messages: textMessages
							}
						});
					});
				});
			}
			else
			{
				resolve(
				{
					status: '401',
					body: {error: 'invalid username or password'}
				});
			}
		});
	});
}

MessageController.prototype.getPopularMessages = function(username, password, type)
{
	var db = this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			if(userId != null)
			{
				db.getPopularMessages(type).then(function(popularMessages)
				{
					resolve(
					{
						status: 200, 
						body:
						{
							messages: popularMessages
						}
					});
				});
			}
			else
			{
				resolve(
				{
					status: '401',
					body: {error: 'invalid username or password'}
				});
			}
		});
	});
}

module.exports =MessageController;