var mysql = require('mysql');

var connection = mysql.createConnection(
{
	host: 'nj20.host.cs.st-andrews.ac.uk',
	user: 'nj20',
	password: 'u3.C1R8jP0Mhj5',
	database: 'nj20_cs3101_db'
});
connection.connect();

var DatabaseController =function()
{
}

DatabaseController.prototype.addTextMessage = function(messageId, textMessage)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'INSERT INTO TextMessage (idMessage, text) VALUES (?, ?);',
			values:[messageId, textMessage]
		}, function(err, rows, field)
		{
			if(err ==null)
			{
				resolve(true);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.addUser = function(userId, username, email, age, password)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'INSERT INTO User (idUser, name, password, email, age) VALUES (?, ?, ?, ?, ?);',
			values:[userId, username, password, email, age]
		}, function(err, rows, field)
		{
			if(err ==null)
			{
				resolve(true);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.modifyTextMessage = function(messageId, textMessage)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'UPDATE TextMessage SET idMessage=?, text=? WHERE idMessage=?;',
			values:[messageId, textMessage, messageId]
		}, function(err, rows, field)
		{
			if(err ==null)
			{
				resolve(true);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.addMessage = function(userId, locationId, messageId, messageDate)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'INSERT INTO Message (idMessage, `date`, idLocation, idUser) VALUES (?, ?, ?, ?);',
			values:[messageId, messageDate, locationId, userId]
		}, function(err, rows, field)
		{
			console.log(err);
			if(err == null)
			{
				resolve();
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.getMessage = function(messageId)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'SELECT * FROM Message WHERE idMessage=?',
			values:[messageId]
		}, function(err, rows, field)
		{
			if(err == null)
			{
				resolve(rows[0]);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.deleteMessage = function(messageId, type)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'DELETE FROM ' + type + ' WHERE idMessage=?',
			values:[messageId]
		}, function(err, rows, field)
		{
			if(err == null)
			{
				connection.query(
				{
					sql: 'DELETE FROM Message WHERE idMessage=?',
					values:[messageId]
				}, function(err, rows, field)
				{
					if(err == null)
					{
						resolve();
					}
					else
					{
						reject(err);
					}
				});
			}
			else
			{
				reject(err);
			}
		});
	});
} 

DatabaseController.prototype.getMessagesOrderedByDate = function(userId, type)
{
	return new Promise(function(resolve, reject)
	{
		var sql = 'SELECT * FROM Message NATURAL JOIN ' + type + ' NATURAL JOIN User ORDER BY date DESC';
		var values =[];

		if(userId != null && userId != undefined)
		{
			sql = 'SELECT * FROM Message NATURAL JOIN ' + type + ' NATURAL JOIN User WHERE idUser=? ORDER BY date DESC';
			values = [userId]
		}

		connection.query(
		{
			sql: sql,
			values: values
		}, function(err, rows, field)
		{

			resolve(rows);
		});
	});
}

DatabaseController.prototype.getPopularMessages = function(type)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'SELECT * FROM ' + type + ' NATURAL JOIN Message NATURAL JOIN User NATURAL JOIN (SELECT idMessage, COUNT(*) AS likes FROM `Like` GROUP BY idMessage ORDER BY COUNT(*) DESC) AS Popular',
		}, function(err, rows, field)
		{
			if(err ==null)
			{
				resolve(rows);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.addLike = function(userId, messageId, date)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'INSERT INTO `Like` (idUser, idMessage, time) VALUES (?, ?, ?);',
			values:[userId, messageId, new Date(date)]
		}, function(err, rows, field)
		{
			if(err ==null)
			{
				resolve(true);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.removeLike = function(userId, messageId)
{
	return new Promise(function(resolve, reject)
	{
		connection.query(
		{
			sql: 'DELETE FROM `Like` WHERE idUser=? AND idMessage=?',
			values:[userId, messageId]
		}, function(err, rows, field)
		{
			if(err == null)
			{
				resolve(true);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.getUserId = function(username, password)
{
	return new Promise(function(resolve, reject)
	{
		var sql = 'SELECT * FROM User WHERE name = ?';

		if(password != null && password != undefined)
		{
			sql += ' and password = ?';
		}

		connection.query(
		{
			sql: sql,
			values:[username, password]
		}, function(err, rows, fields)
		{
			if(err == null)
			{
				if(rows.length >= 1)
				{
					resolve(rows[0].idUser);
				}
				else
				{
					resolve(null);
				}
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.generateMessageId = function()
{
	return new Promise(function(resolve, reject)
	{
		connection.query({sql: 'SELECT MAX(idMessage) AS maxId FROM Message'}, function(err, rows, fields)
		{
			if(err ==null)
			{
				resolve(parseInt(rows[0]['maxId'])+1);
			}
			else
			{
				reject(err);
			}
		});
	});
}

DatabaseController.prototype.generateUserId = function()
{
	return new Promise(function(resolve, reject)
	{
		connection.query({sql: 'SELECT MAX(idUser) AS maxId FROM User'}, function(err, rows, fields)
		{
			if(err ==null)
			{
				resolve(parseInt(rows[0]['maxId'])+1);
			}
			else
			{
				reject(err);
			}
		});
	});
}

module.exports = DatabaseController;

