var UserController = function(db)
{
	this.db = db;
}

UserController.prototype.addUser =function(username, email, age, password)
{
	var db =this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			if(userId == null)
			{
				db.generateUserId().then(function(newUserId)
				{
					db.addUser(newUserId, username, email, age, password).then(function()
					{
						resolve(
						{
							status: 201,
							body: 
							{
								username: username,
								email: email,
								age: age
							}
						});
					});
				});
			}
			else
			{
				resolve(
				{
					status: 409,
					body: {error: 'Username already exists'}
				});
			}	
		});
	});
}

UserController.prototype.userExists =function(username, password)
{
	var db =this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			if(userId == null)
			{
				resolve(
				{
					status:200,
					body: {exists: false}
				})
			}
			else
			{
				resolve(
				{
					status:200,
					body: {exists: true}
				})
			}	
		});
	});
}

module.exports = UserController;
