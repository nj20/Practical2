var LikeController = function(db)
{
	this.db = db;
}

LikeController.prototype.addLike = function(username, password, messageId, date)
{
	var db = this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			if(userId != null)
			{
				db.addLike(userId, messageId, date).then(function()
				{
					resolve(
					{
						status: 201, 
						body:
						{
							username: username, 
							idMessage: messageId,
							date: date
						}
					});
				});
			}
			else
			{
				resolve(
				{
					status: '401',
					body: {error: 'invalid username or password'}
				});
			}
		});
	});
}

LikeController.prototype.removeLike = function(username, password, messageId)
{
	var db = this.db;
	return new Promise(function(resolve, reject)
	{
		db.getUserId(username, password).then(function(userId)
		{
			if(userId != null)
			{
				db.removeLike(userId, messageId).then(function()
				{
					resolve(
					{
						status: 200, 
						body:
						{
							username: username, 
							idMessage: messageId,
						}
					});
				});
			}
			else
			{
				resolve(
				{
					status: '401',
					body: {error: 'invalid username or password'}
				});
			}
		});
	});
}

module.exports = LikeController;