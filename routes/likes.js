var express = require('express');
var router = express.Router();

var DatabaseController = new require('../controllers/databaseController');
var LikeController = new require('../controllers/likeController');

var db = new DatabaseController();
var likeController = new LikeController(db);

router.post('/', function(req, res, next) 
{
	var body = req.body;
	likeController
	.addLike(req.headers['username'], req.headers['password'], body.idMessage, body.date)
	.then(function(response)
	{
		res.status(response.status)
		res.send(response.body);
	});	
});

router.delete('/', function(req, res, next) 
{
	var body = req.body;

	likeController
	.removeLike(req.headers['username'], req.headers['password'], body.idMessage)
	.then(function(response)
	{
		res.status(response.status)
		res.send(response.body);
	});	
});

module.exports = router;
