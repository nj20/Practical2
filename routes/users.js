var express = require('express');
var router = express.Router();

var DatabaseController = new require('../controllers/databaseController');
var UserController = new require('../controllers/userController');

var db = new DatabaseController();
var userController = new UserController(db);

/* GET users listing. */
router.post('/', function(req, res, next)
{
	userController
	.addUser(req.body.username, req.body.email, req.body.age, req.body.password).then(function(response)
	{
		console.log(response.body);
		res.status(response.status);
		res.send(response.body);
	});
});

router.get('/exists', function(req, res, next)
{
	userController
	.userExists(req.headers['username'], req.headers['password']).then(function(response)
	{
		res.status(response.status);
		res.send(response.body);
	});
});

module.exports = router;
