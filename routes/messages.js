var express = require('express');
var router = express.Router();

var DatabaseController = new require('../controllers/databaseController');
var MessageController = new require('../controllers/messageController');

var db = new DatabaseController();
var messageController = new MessageController(db);

router.post('/TextMessage', function(req, res, next) 
{
	var body = req.body;
	messageController
	.postTextMessage(req.headers['username'], req.headers['password'], body.locationId, body.textMessage, body.date)
	.then(function(response)
	{
		res.status(response.status)
		res.send(response.body);
	});	
});

router.patch('/TextMessage', function(req, res, next) 
{
	var body = req.body;
	messageController
	.modifyTextMessage(req.headers['username'], req.headers['password'], req.body.idMessage, body.textMessage)
	.then(function(response)
	{
		res.status(response.status)
		res.send(response.body);
	});	
});

router.delete('/:messageType', function(req, res, next) 
{
	messageController
	.deleteMessage(req.headers['username'], req.headers['password'], req.body.idMessage, req.params.messageType)
	.then(function(response)
	{
		res.status(response.status);
		res.send(response.body);
	});
});

router.get('/orderedByDate/:messageType', function(req, res, next) 
{
	var body = req.body;

	messageController
	.getMessagesOrderedByDate(req.headers['username'], req.headers['password'], req.params.messageType, null)
	.then(function(response)
	{
		res.status(response.status);
		res.send(response.body);
	});	
});

router.get('/orderedByDate/:messageType/:username', function(req, res, next) 
{
	var body = req.body;

	messageController
	.getMessagesOrderedByDate(req.headers['username'], req.headers['password'], req.params.messageType, req.params.username)
	.then(function(response)
	{
		res.status(response.status);
		res.send(response.body);
	});	
});

router.get('/orderedByLikes/:messageType', function(req, res, next) 
{
	console.log("COMES");
	var body = req.body;

	messageController
	.getPopularMessages(req.headers['username'], req.headers['password'], req.params.messageType)
	.then(function(response)
	{
		res.status(response.status);
		res.send(response.body);
	});	
});

module.exports = router;
