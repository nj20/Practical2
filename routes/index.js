var express = require('express');
var router = express.Router();

var DatabaseController = new require('../controllers/databaseController');
var MessageController = new require('../controllers/messageController');

var db = new DatabaseController();
var messageController = new MessageController(db);


/* GET home page. */
router.get('/', function(req, res, next) 
{
  res.render('index');
});

router.get('/signin', function(req, res, next) 
{
  res.render('signin');
});

router.get('/register', function(req, res, next) 
{
  res.render('register');
});

router.get('/recentMessages', function(req, res, next) 
{
  res.render('recentMessages');
});

router.get('/popularMessages', function(req, res, next) 
{
  res.render('popularMessages');
});

router.get('/viewByUser', function(req, res, next) 
{
  res.render('viewByUser');
});


module.exports = router;
