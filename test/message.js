var request = require('supertest'),
	expect = require('chai').expect,
	app = request(require('../app'))

describe("Messages", function() 
{
    it("can be added", function(done) 
    {
    	app
    	.post("/messages/TextMessage")
    	.set('username','Bob')
    	.set('password','1234')
    	.send({locationId: '2', textMessage: 'This is a message', date: new Date()})
    	.expect(201)
    	.end(done);
    });

    it("cannot be added with invalid username", function(done) 
    {
    	app
    	.post("/messages/TextMessage")
    	.set('username','123413543')
    	.set('password','1234')
    	.send({locationId: '2', textMessage: 'This is a message', date: new Date()})
    	.expect(401)
    	.end(done);
    });

    it("can be deleted", function(done)
    {
        app
        .post("/messages/TextMessage")
        .set('username','Bob')
        .set('password','1234')
        .send({locationId: '2', textMessage: 'This is a message', date: new Date()})
        .expect(201)
        .end(function(err, res)
        {
            app
            .delete("/messages/TextMessage")
            .set('username','Bob')
            .set('password','1234')
            .send({idMessage:res.body.idMessage})
            .expect(200)
            .end(done);
        });
    });

       it("can be edited", function(done)
    {
        app
        .post("/messages/TextMessage")
        .set('username','Bob')
        .set('password','1234')
        .send({locationId: '2', textMessage: 'This is a message', date: new Date()})
        .expect(201)
        .end(function(err, res)
        {
            app
            .patch("/messages/TextMessage")
            .set('username','Bob')
            .set('password','1234')
            .send({idMessage:res.body.idMessage, textMessage: "Edited Message"})
            .expect(200)
            .end(done);
        });
    });
});


describe("All Text Messages", function() 
{
    it("can be retrieved ordered by date", function(done) 
    {
    	app
    	.get("/messages/orderedByDate/TextMessage")
    	.set('username','Bob')
    	.set('password','1234')
    	.expect(200)
    	.end(done);
    });

     it("can be retrieved ordered by date by a user", function(done) 
    {
        app
        .get("/messages/orderedByDate/TextMessage/2")
        .set('username','Bob')
        .set('password','1234')
        .expect(200)
        .end(done);

    });

     it("can be retrieved ordered by likes", function(done) 
    {
        app
        .get("/messages/orderedByLikes/TextMessage")
        .set('username','Bob')
        .set('password','1234')
        .expect(200)
        .end(done);
    });
});