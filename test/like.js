var request = require('supertest'),
	expect = require('chai').expect,
	app = request(require('../app'))

describe("Likes", function() 
{
    it("can be added", function(done) 
    {
    	app
        .post("/messages/TextMessage")
        .set('username','Bob')
        .set('password','1234')
        .send({locationId: '2', textMessage: 'This is a message', date: new Date()})
        .expect(201)
        .end(function(err, res)
        {
            app
            .post("/likes/")
            .set('username','Bob')
            .set('password','1234')
            .send({idMessage:res.body.idMessage, date: new Date()})
            .expect(201)
            .end(done);
        });
    });

    it("can be deleted", function(done) 
    {
        app
        .post("/messages/TextMessage")
        .set('username','Bob')
        .set('password','1234')
        .send({locationId: '2', textMessage: 'This is a message', date: new Date()})
        .expect(201)
        .end(function(err, res)
        {
            app
            .post("/likes/")
            .set('username','Bob')
            .set('password','1234')
            .send({idMessage:res.body.idMessage, date: new Date()})
            .expect(201)
            .end(function(err, res2)
            {
                 app
                .delete("/likes/")
                .set('username','Bob')
                .set('password','1234')
                .send({idMessage:res.body.idMessage})
                .expect(200)
                .end(done);
            });
        });
    });
});