var listAllMessages =function(username, edit)
{
	$.ajax(
    {
	    url: "messages/orderedByDate/TextMessage/" + username,
	    type: "GET",
	    headers: {"username": getCookie("username"), "password": getCookie("password")},
	    success: function(data)
	    {
	    	for(var count = 0; count < data.messages.length; count++)
    		{
    			renderMessage(data.messages[count].name, data.messages[count].idMessage, data.messages[count].text, data.messages[count].date, edit);
    		}
	    }
	});
}

var updateIndex =function(username, edit)
{
	if(document.cookie != "")
	{
		clearMessages();
		listAllMessages(username, edit);
	}
}
