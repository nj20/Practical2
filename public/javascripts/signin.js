function signin()
{ 
	var name = document.getElementById("inputName").value;
	var password = document.getElementById("inputPassword").value;

    $.ajax(
    {
	    url: "users/exists",
	    type: "GET",
	    headers: {"username": name, "password": password},
	    success: function(data)
	    {
	    	if(data.exists)
	    	{
		    	setCookie("username", name); 
				setCookie("password", password); 
				window.location = "/";
			}
	    }
	});
}