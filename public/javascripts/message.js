var renderMessage = function(user, idMessage, textMessage, date, edit)
{
	if(edit != null)
	{
		document.getElementById("messages").innerHTML +=
		'<div class="jumbotron">' + 
			'<div class="insideJumbotron">' + 
				'<h2>' + user + '</h2>' + 
				'<p>' + textMessage + '</p>' + 
				'<button class = "glyphicon glyphicon-thumbs-up" onclick="addLike(' + idMessage + ')"></button>' +
				'<button class = "glyphicon glyphicon-thumbs-down" onclick="removeLike(' + idMessage + ')"></button>' +
				'<p></p>' +
				'<small>' + (new Date(date)).toDateString() + '</small>' + 
				'</br>' +
				'</br>' +
				'<button type="button" class="btn btn-default" aria-label="Left Align" onclick="editMessage(this, ' + idMessage + ')">' + 
				'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>' + 
				'</button>'
			'</div>' + 
		'</div>';
	}
	else
	{
		document.getElementById("messages").innerHTML +=
		'<div class="jumbotron">' + 
			'<div class="insideJumbotron">' + 
				'<h2>' + user + '</h2>' + 
				'<p>' + textMessage + '</p>' + 
				'<button class = "glyphicon glyphicon-thumbs-up" onclick="addLike(' + idMessage + ')"></button>' +
				'<button class = "glyphicon glyphicon-thumbs-down" onclick="removeLike(' + idMessage + ')"></button>' +
				'<p></p>' +
				'<small>' + (new Date(date)).toDateString() + '</small>' + 
			'</div>' + 
		'</div>';
	}
}

var clearMessages = function()
{
	document.getElementById("messages").innerHTML = "";
}

var addMessage = function(text, date)
{
	$.ajax(
    {
	    url: "messages/TextMessage",
	    type: "POST",
	    headers: {"username": getCookie("username"), "password": getCookie("password")},
	    data: {locationId: 1, textMessage: text, date: date},
	    success: function(data)
	    {
		    window.location = "/";
	    }
	});
}

var editMessage = function(domElement, idMessage)
{
	domElement = domElement.parentNode;
	if(domElement.childNodes.length == 9)
	{
		var element = document.createElement('input');
		element.setAttribute("type", "text");
		element.setAttribute("class", "form-control");
		element.setAttribute("id", "editMessage");
		element.setAttribute("onKeyup", "editMessageInDataBase(this, " + idMessage + ")");
		domElement.appendChild(element);
	}
}

var editMessageInDataBase = function(domElement, idMessage)
{
	if(window.event.keyCode == 13)
	{
		$.ajax(
	    {
		    url: "messages/TextMessage",
		    type: "PATCH",
		    headers: {"username": getCookie("username"), "password": getCookie("password")},
		    data: {idMessage: idMessage, textMessage: domElement.value},
		    success: function(data)
		    {
		    	window.location = "/";
		    }
		});
	}
}

var addLike = function(idMessage)
{
	$.ajax(
    {
	    url: "likes/",
	    type: "POST",
	    headers: {"username": getCookie("username"), "password": getCookie("password")},
	    data: {idMessage: idMessage, date: new Date()},
	    success: function(data)
	    {
	    }
	});
}

var removeLike = function(idMessage)
{
	$.ajax(
    {
	    url: "likes/",
	    type: "DELETE",
	    headers: {"username": getCookie("username"), "password": getCookie("password")},
	    data: {idMessage: idMessage},
	    success: function(data)
	    {
	    }
	});
}