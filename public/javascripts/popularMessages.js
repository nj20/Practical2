var listAllMessages =function()
{
	$.ajax(
    {
	    url: "messages/orderedByLikes/TextMessage",
	    type: "GET",
	    headers: {"username": getCookie("username"), "password": getCookie("password")},
	    success: function(data)
	    {
	    	for(var count = 0; count < data.messages.length; count++)
    		{
    			renderMessage(data.messages[count].name, data.messages[count].idMessage, data.messages[count].text, data.messages[count].date);
    		}
	    }
	});
}

var updateIndex =function()
{
	if(document.cookie != "")
	{
		listAllMessages();
	}
}

updateIndex();